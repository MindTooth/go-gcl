package main

import "github.com/xanzy/go-gitlab"

func GetGitLabProjects(client *gitlab.Client, includeArchived bool) (projects []*gitlab.Project, err error) {
	projects, _, err = client.Projects.ListProjects(&gitlab.ListProjectsOptions{Owned: gitlab.Bool(true), Archived: &includeArchived}, nil)

	return projects, err
}
