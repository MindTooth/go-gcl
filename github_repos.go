package main

import (
	"context"

	"github.com/google/go-github/v50/github"
)

func GetPublicGitHubRepos(client *github.Client, ctx context.Context, includeArchived bool) (repos []*github.Repository, err error) {
	repos, _, err = client.Repositories.List(ctx, "", nil)

	return repos, err
}
