package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"sync"

	"github.com/charmbracelet/bubbles/spinner"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/muesli/reflow/indent"
	"github.com/muesli/termenv"
	"github.com/xanzy/go-gitlab"
)

var (
	color       = termenv.ColorProfile().Color
	wg          sync.WaitGroup
	currentPath string
	glToken     = os.Getenv("GITLAB_TOKEN")
	err         error
)

func main() {
	currentPath, err = os.Getwd()
	if err != nil {
		log.Println("Hel")
	}

	p := tea.NewProgram(initialModel())
	if err := p.Start(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

/* Main Model */

type model struct {
	spinner    spinner.Model
	quitting   bool
	err        error
	ready      bool
	chosen     bool
	choice     *gitlab.Project
	cursor     int
	glSync     *chan bool
	pathInput  textinput.Model
	glProjects []*gitlab.Project
}

func initialModel() model {
	s := spinner.NewModel()
	s.Spinner = spinner.Dot

	ti := textinput.NewModel()
	ti.Focus()
	ti.CharLimit = 156
	ti.CursorEnd()

	return model{spinner: s, pathInput: ti, err: nil}
}

/* GitLab Integration */

func getGitLabProjects() tea.Cmd {
	return func() tea.Msg {
		glClient, err := gitlab.NewClient(glToken)
		if err != nil {
			log.Fatalf("Failed to create client: %v", err)
			return errMsg{err}
		}

		projects, err := GetGitLabProjects(glClient, false)
		if err != nil {
			return err
		}

		return gitlabResponseMsg{projects}
	}
}

type gitlabResponseMsg struct {
	gitlabProjects []*gitlab.Project
}

/* Error */

type errMsg struct{ err error }

func (e errMsg) Error() string { return e.err.Error() }

/* Bubbletea - Init */

func (m model) Init() tea.Cmd {
	return tea.Batch(
		spinner.Tick,
		getGitLabProjects(),
	)
}

/* Bubbletea - Update */

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyCtrlC:
			fallthrough
		case tea.KeyEsc:
			return m, tea.Quit
		}
	case spinner.TickMsg:
		var cmd tea.Cmd
		m.spinner, cmd = m.spinner.Update(msg)
		return m, cmd
	case gitlabResponseMsg:
		m.glProjects = msg.gitlabProjects
		m.ready = true
		return m, nil
	case errMsg:
		m.err = msg
		m.quitting = true
		return m, tea.Quit
	}

	if m.ready && !m.chosen {
		return updateList(msg, m)
	}

	return updatePath(msg, m)
}

func updateList(msg tea.Msg, m model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {

	case tea.KeyMsg:
		switch msg.String() {
		case "enter":
			// Send the choice on the channel and exit.
			if len(m.glProjects) > 0 {
				m.choice = m.glProjects[m.cursor]
				m.chosen = true
				m.pathInput.SetValue(fmt.Sprintf("%s/%s", currentPath, m.choice.Path))
				m.pathInput.CursorEnd()
			}

			return m, nil
		case "down", "j":
			m.cursor++
			if m.cursor >= len(m.glProjects) {
				m.cursor = 0
			}

		case "up", "k":
			m.cursor--
			if m.cursor < 0 {
				m.cursor = len(m.glProjects) - 1

			}
		default:
			return m, nil
		}
	}

	return m, nil
}

func updatePath(msg tea.Msg, m model) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyCtrlC:
			fallthrough
		case tea.KeyEsc:
			fallthrough
		case tea.KeyEnter:
			return m, tea.Quit
		}

	// We handle errors just like any other message
	case errMsg:
		m.err = msg
		return m, nil
	}

	m.pathInput, cmd = m.pathInput.Update(msg)
	return m, cmd
}

/* Bubbletea - View */

func (m model) View() string {
	var s string

	if m.quitting {
		return "\n  See you later!\n\n"
	}

	if !m.ready {
		s = spinnerView(m)
	} else if !m.chosen {
		s = listView(m)
	} else {
		s = pathView(m)
	}

	return indent.String("\n"+s+"\n\n", 2)
}

func pathView(m model) string {
	return fmt.Sprintf(
		"Choose which path to clone to?\n\n%s\n\n%s",
		m.pathInput.View(),
		"(esc to quit)",
	) + "\n"
}

func spinnerView(m model) string {
	if m.err != nil {
		return m.err.Error()
	}
	s := termenv.String(m.spinner.View()).Foreground(color("205")).String()
	str := fmt.Sprintf("\n\n   %s Loading forever...press esc to quit\n\n", s)
	if m.quitting {
		return str + "\n"
	}
	return str
}

func listView(m model) string {
	s := strings.Builder{}
	s.WriteString("What kind of Bubble Tea would you like to order?\n\n")

	for i := 0; i < len(m.glProjects); i++ {
		if m.cursor == i {
			s.WriteString("(•) ")
		} else {
			s.WriteString("( ) ")
		}
		s.WriteString(m.glProjects[i].NameWithNamespace)
		s.WriteString(" ")
		s.WriteString(``)
		s.WriteString("\n")
	}
	s.WriteString("\n(press esc to quit)\n")

	return s.String()
}
